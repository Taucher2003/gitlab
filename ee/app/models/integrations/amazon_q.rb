# frozen_string_literal: true

module Integrations
  class AmazonQ < Integration
    include Integrations::Base::AmazonQ
  end
end
